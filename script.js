let csvData = []; // Initialize csvData as an array
let columnTitles = []; // Initialize columnTitles as an array
let loadedFileName = ''; // Variable to store the name of the loaded file
let sortOrders = []; // Array to keep track of sort order for each column

document.getElementById('fileInput').addEventListener('change', function(e) {
    const file = e.target.files[0];
    if (!file) {
        return;
    }

    // 로드된 파일 이름을 저장
    loadedFileName = file.name;

    const reader = new FileReader();
    reader.onload = function(e) {
        const csv = e.target.result;
        parseCSV(csv); // CSV 데이터를 파싱하여 처리
        displayCSV(); // CSV 데이터를 표시하고 파일 이름을 전달
		document.title = `${loadedFileName}`; // HTML 제목 설정
    };

    reader.readAsText(file);
});

function parseCSV(csv) {
    const rows = csv.split(/\r?\n/); // 정규 표현식을 사용하여 줄 바꿈 문자를 처리합니다.
    columnTitles = parseRow(rows[0]); // 첫 번째 줄에서 열 제목을 추출합니다.
    sortOrders = new Array(columnTitles.length).fill('asc'); // Initialize sortOrders array with default values (ascending)
    csvData = [];

    for (let i = 1; i < rows.length; i++) {
        const rowData = parseRow(rows[i]);
        csvData.push(rowData);
    }

    function parseRow(row) {
        const result = [];
        let inQuotes = false;
        let value = '';

        for (let char of row) {
            if (char === '"') {
                inQuotes = !inQuotes; // 따옴표가 닫히거나 열리면 상태를 전환합니다.
            } else if (char === ',' && !inQuotes) {
                result.push(value.trim()); // 따옴표 밖의 콤마를 구분자로 처리합니다.
                value = '';
            } else {
                value += char; // 나머지 문자들은 value에 추가합니다.
            }
        }

        result.push(value.trim()); // 마지막 값을 추가합니다.
        return result.map(data => data.replace(/\r/g, '\n').replace(/\\n/g, '\n')); // 줄바꿈 문자를 처리합니다.
    }
}

// Function to display CSV data


function displayCSV() {
    const table = document.createElement('table');
    table.style.borderCollapse = 'collapse'; // 테이블의 테두리를 합쳐서 보이게 함

    // Creating table headers with column titles and sorting icons
    const headerRow = document.createElement('tr');
    columnTitles.forEach((title, index) => {
        const th = document.createElement('th');
        th.textContent = title;
        th.style.backgroundColor = '#f2f2f2'; // 헤더 배경색
        th.style.border = '1px solid #ddd'; // 테두리
        th.style.padding = '8px';

        // Create sorting icon
        const sortIcon = document.createElement('span');
        sortIcon.className = 'sort-icon';
        sortIcon.textContent = '⇅';
        sortIcon.dataset.columnIndex = index;
        sortIcon.addEventListener('click', handleSort);
        th.appendChild(sortIcon);

        // Create input element for filtering
        const filterInput = document.createElement('input');
        filterInput.type = 'text';
        filterInput.placeholder = `Filter ${title}`;
        filterInput.dataset.columnIndex = index;
        filterInput.addEventListener('input', handleFilter);
        th.appendChild(filterInput);

        headerRow.appendChild(th);
    });

    // Add Action column header
    const actionHeader = document.createElement('th');
    actionHeader.textContent = 'Action';
    actionHeader.style.backgroundColor = '#f2f2f2';
    actionHeader.style.border = '1px solid #ddd';
    actionHeader.style.padding = '8px';
    headerRow.appendChild(actionHeader);

    table.appendChild(headerRow);

    // Displaying the data rows
    csvData.forEach((rowData, rowIndex) => {
        const row = document.createElement('tr');
        row.style.backgroundColor = rowIndex % 2 === 0 ? '#ffffff' : '#f9f9f9'; // 행 배경색 교차 적용

        rowData.forEach((cellData, cellIndex) => {
            const cell = document.createElement('td');
            const spanContainer = document.createElement('div');

            // Parse the cell data for URLs and text
            const parts = parseCellData(cellData);

            parts.forEach(part => {
                const span = document.createElement('span');
                span.textContent = part.text;
                span.dataset.rowIndex = rowIndex;
                span.dataset.cellIndex = cellIndex;

                if (part.type === 'url') {
                    span.style.color = 'blue';
                    span.style.textDecoration = 'underline';
                    span.style.cursor = 'pointer';
                    span.addEventListener('click', () => {
                        window.open(part.text, '_blank');
                    });
                } else if (part.type === 'file') {
                    span.style.color = 'blue';
                    span.style.textDecoration = 'underline';
                    span.style.cursor = 'pointer';
                    span.addEventListener('click', () => {
                        alert('File path: ' + part.text); // 실제 파일을 여는 로직은 환경에 따라 다르므로, 여기서는 경로를 알림으로 표시합니다.
                    });
                } else {
                    span.addEventListener('click', () => handleViewRow(rowIndex));
                }

                // Set maximum height for each cell content to limit to two lines
                span.style.display = '-webkit-box';
                span.style.webkitBoxOrient = 'vertical';
                span.style.webkitLineClamp = 2;
                span.style.overflow = 'hidden';

                spanContainer.appendChild(span);
            });

            cell.appendChild(spanContainer);
            cell.style.border = '1px solid #ddd'; // 테두리
            cell.style.padding = '8px'; // 패딩
            row.appendChild(cell);
        });

        // Add action buttons to each row in a single cell
        const actionCell = document.createElement('td');
        actionCell.style.border = '1px solid #ddd';
        actionCell.style.padding = '8px';
        actionCell.style.display = 'flex'; // 가로로 버튼 배치
        actionCell.style.gap = '5px'; // 버튼 사이 간격

        //const editButton = document.createElement('button');
        //editButton.textContent = '수정';
        //editButton.addEventListener('click', () => handleEditRow(rowIndex));
        //actionCell.appendChild(editButton);

        const copyButton = document.createElement('button');
        copyButton.textContent = '복사';
        copyButton.addEventListener('click', () => handleCopyRow(rowIndex));
        actionCell.appendChild(copyButton);

        const deleteButton = document.createElement('button');
        deleteButton.textContent = '삭제';
        deleteButton.addEventListener('click', () => handleDeleteRow(rowIndex));
        actionCell.appendChild(deleteButton);

        row.appendChild(actionCell);

        table.appendChild(row);
    });

    // Clear and append the table to the CSV data div
    const csvDiv = document.getElementById('csvData');
    csvDiv.innerHTML = '';
    csvDiv.appendChild(table);
}






// Helper function to parse cell data for URLs and text
function parseCellData(cellData) {
    const urlPattern = /(https?:\/\/[^\s]+)/g;
    const filePattern = /[a-zA-Z]:\\[\\a-zA-Z0-9._-]+/g;
    const parts = [];

    let match;
    let lastIndex = 0;

    while ((match = urlPattern.exec(cellData)) !== null) {
        if (match.index > lastIndex) {
            parts.push({ text: cellData.substring(lastIndex, match.index), type: 'text' });
        }
        parts.push({ text: match[0], type: 'url' });
        lastIndex = match.index + match[0].length;
    }

    while ((match = filePattern.exec(cellData)) !== null) {
        if (match.index > lastIndex) {
            parts.push({ text: cellData.substring(lastIndex, match.index), type: 'text' });
        }
        parts.push({ text: match[0], type: 'file' });
        lastIndex = match.index + match[0].length;
    }

    if (lastIndex < cellData.length) {
        parts.push({ text: cellData.substring(lastIndex), type: 'text' });
    }

    return parts;
}

// Helper function to check if a string is a valid URL
function isValidURL(str) {
    const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

// Helper function to check if a string is a valid file path
function isValidFilePath(str) {
    // 간단한 파일 경로 유효성 검사
    const pattern = /^[a-zA-Z]:\\[\\a-zA-Z0-9._-]+$/;
    return pattern.test(str);
}


function handleFilter(event) {
    // Loop through each row to hide or show based on filter
    csvData.forEach((rowData, rowIndex) => {
        let isMatch = true; // Flag to check if all columns match the filter
        rowData.forEach((cellData, columnIndex) => {
            const filterText = document.querySelector(`input[data-column-index="${columnIndex}"]`).value.toLowerCase();
            const cellValue = cellData.toLowerCase();

            // If filter text is not empty, check if cell data includes filter text
            if (filterText !== "") {
                if (!cellValue.includes(filterText)) {
                    isMatch = false; // If any cell does not match, set the flag to false
                }
            }
        });

        const row = document.querySelector(`table tr:nth-child(${rowIndex + 2})`); // rowIndex는 0부터 시작하므로 +2를 해줍니다.

        // Show row if all columns match the filter or if filter text is empty for all columns
        if (isMatch || event.target.value.trim() === "") {
            row.style.display = "";
        } else {
            row.style.display = "none"; // Hide row if any cell does not match the filter
        }
    });
}

function handleSort(event) {
    const columnIndex = parseInt(event.target.dataset.columnIndex);
    const order = sortOrders[columnIndex];
    const isAscending = order === 'asc';
    const sortedData = csvData.slice().sort((a, b) => {
        const valueA = a[columnIndex];
        const valueB = b[columnIndex];
        if (valueA < valueB) {
            return isAscending ? -1 : 1;
        }
        if (valueA > valueB) {
            return isAscending ? 1 : -1;
        }
        return 0;
    });

    // Toggle sort order
    sortOrders[columnIndex] = isAscending ? 'desc' : 'asc';

    // Update displayed CSV with sorted data
    csvData = sortedData;
    displayCSV();
}

function handleViewRow(rowIndex) {
    const rowData = csvData[rowIndex];

    // Create a temporary div to measure the height of the form content
    const tempDiv = document.createElement('div');
    tempDiv.style.position = 'absolute';
    tempDiv.style.visibility = 'hidden';
    tempDiv.style.width = '580px'; // Account for padding and border in the final form
    tempDiv.style.padding = '10px';
    tempDiv.style.boxSizing = 'border-box';

    rowData.forEach((cellData, cellIndex) => {
        const label = document.createElement('label');
        label.style.fontWeight = 'bold';
        label.style.display = 'block';
        label.style.marginBottom = '5px';
        label.textContent = `${columnTitles[cellIndex]}: `;

        const input = document.createElement('div');
        input.textContent = cellData;
        input.style.width = '100%';
        input.style.padding = '5px';
        input.style.border = '1px solid #ccc';
        input.style.marginBottom = '10px';
        input.style.backgroundColor = '#f9f9f9';
        input.style.whiteSpace = 'pre-wrap'; // Preserve line breaks
        input.style.boxSizing = 'border-box'; // Include padding and border in the element's total width and height

        // Check for URLs and file paths and make them clickable
        const parts = parseCellData(cellData);
        input.innerHTML = ''; // Clear textContent to append parts

        parts.forEach(part => {
            if (part.type === 'url' || part.type === 'file') {
                const link = document.createElement('a');
                link.href = part.text;
                link.textContent = part.text;
                link.style.color = 'blue';
                link.style.textDecoration = 'underline';
                link.target = '_blank';
                input.appendChild(link);
            } else {
                const span = document.createElement('span');
                span.textContent = part.text;
                input.appendChild(span);
            }
        });

        tempDiv.appendChild(label);
        tempDiv.appendChild(input);
    });

    // Add edit button at the top
    const editButton = document.createElement('button');
    editButton.textContent = '수정';
    editButton.type = 'button';
    editButton.style.marginBottom = '10px';
    tempDiv.appendChild(editButton);

    // Close button
    const closeButton = document.createElement('button');
    closeButton.textContent = 'Close';
    closeButton.type = 'button';
    closeButton.style.marginTop = '10px';
    tempDiv.appendChild(closeButton);

    document.body.appendChild(tempDiv);
    const formHeight = tempDiv.offsetHeight;
    document.body.removeChild(tempDiv);

    // Open a new window for viewing with the calculated height
    const viewWindow = window.open('', 'View Row', `width=1000,height=${formHeight + 40}`);
    viewWindow.document.body.style.margin = '0';
    viewWindow.document.body.style.overflow = 'hidden'; // Prevent body scrollbars

    const form = document.createElement('form');
    form.style.padding = '10px';
    form.style.boxSizing = 'border-box';
    form.style.overflowY = 'auto';
    form.style.overflowX = 'hidden'; // Prevent horizontal scroll bar
    form.style.height = 'calc(100% - 50px)'; // Adjust height to fit within the window, leaving space for the close button

    // Adjust form size dynamically with window size
    const resizeForm = () => {
        form.style.width = (viewWindow.innerWidth - 20) + 'px';
        form.style.height = (viewWindow.innerHeight - 20) + 'px';
    };
    viewWindow.addEventListener('resize', resizeForm);

    // Add edit button at the top
    editButton.addEventListener('click', () => {
        viewWindow.close(); // Close the view window
        handleEditRow(rowIndex); // Open the edit window
    });
    form.appendChild(editButton);

    rowData.forEach((cellData, cellIndex) => {
        const label = document.createElement('label');
        label.style.fontWeight = 'bold';
        label.style.display = 'block';
        label.style.marginBottom = '5px';
        label.textContent = `${columnTitles[cellIndex]}: `;

        const input = document.createElement('div');
        input.textContent = cellData;
        input.style.width = '100%';
        input.style.padding = '5px';
        input.style.border = '1px solid #ccc';
        input.style.marginBottom = '10px';
        input.style.backgroundColor = '#f9f9f9';
        input.style.whiteSpace = 'pre-wrap'; // Preserve line breaks
        input.style.boxSizing = 'border-box'; // Include padding and border in the element's total width and height

        // Check for URLs and file paths and make them clickable
        const parts = parseCellData(cellData);
        input.innerHTML = ''; // Clear textContent to append parts

        parts.forEach(part => {
            if (part.type === 'url' || part.type === 'file') {
                const link = document.createElement('a');
                link.href = part.text;
                link.textContent = part.text;
                link.style.color = 'blue';
                link.style.textDecoration = 'underline';
                link.target = '_blank';
                input.appendChild(link);
            } else {
                const span = document.createElement('span');
                span.textContent = part.text;
                input.appendChild(span);
            }
        });

        form.appendChild(label);
        form.appendChild(input);
    });

    // Close button
    closeButton.addEventListener('click', () => {
        viewWindow.close();
    });
    form.appendChild(closeButton);

    viewWindow.document.body.appendChild(form);
    resizeForm(); // Initial call to set the form size correctly
}


// Helper function to parse cell data for URLs and text
function parseCellData(cellData) {
    const urlPattern = /(https?:\/\/[^\s]+)/g;
    const filePattern = /[a-zA-Z]:\\[\\a-zA-Z0-9._-]+/g;
    const parts = [];

    let match;
    let lastIndex = 0;

    while ((match = urlPattern.exec(cellData)) !== null) {
        if (match.index > lastIndex) {
            parts.push({ text: cellData.substring(lastIndex, match.index), type: 'text' });
        }
        parts.push({ text: match[0], type: 'url' });
        lastIndex = match.index + match[0].length;
    }

    while ((match = filePattern.exec(cellData)) !== null) {
        if (match.index > lastIndex) {
            parts.push({ text: cellData.substring(lastIndex, match.index), type: 'text' });
        }
        parts.push({ text: match[0], type: 'file' });
        lastIndex = match.index + match[0].length;
    }

    if (lastIndex < cellData.length) {
        parts.push({ text: cellData.substring(lastIndex), type: 'text' });
    }

    return parts;
}

function handleEditRow(rowIndex) {
    const rowData = csvData[rowIndex];

    // handleViewRow의 window와 동일한 height로 초기 설정
    let viewWindowHeight = 300; // 기본 높이, 실제 handleViewRow에서 가져와야 함
    const viewWindow = window.open('', 'View Row', `width=1000,height=100`); // handleViewRow에서 창을 열어 높이 측정
    const tempDiv = document.createElement('div');
    tempDiv.style.position = 'absolute';
    tempDiv.style.visibility = 'hidden';
    tempDiv.style.width = '580px'; // Account for padding and border in the final form
    tempDiv.style.padding = '10px';
    tempDiv.style.boxSizing = 'border-box';

    rowData.forEach((cellData, cellIndex) => {
        const label = document.createElement('label');
        label.style.fontWeight = 'bold';
        label.style.display = 'block';
        label.style.marginBottom = '5px';
        label.textContent = `${columnTitles[cellIndex]}: `;

        const input = document.createElement('div');
        input.textContent = cellData;
        input.style.width = '100%';
        input.style.padding = '5px';
        input.style.border = '1px solid #ccc';
        input.style.marginBottom = '10px';
        input.style.backgroundColor = '#f9f9f9';
        input.style.whiteSpace = 'pre-wrap'; // Preserve line breaks
        input.style.boxSizing = 'border-box'; // Include padding and border in the element's total width and height

        // Check for URLs and file paths and make them clickable
        const parts = parseCellData(cellData);
        input.innerHTML = ''; // Clear textContent to append parts

        parts.forEach(part => {
            if (part.type === 'url' || part.type === 'file') {
                const link = document.createElement('a');
                link.href = part.text;
                link.textContent = part.text;
                link.style.color = 'blue';
                link.style.textDecoration = 'underline';
                link.target = '_blank';
                input.appendChild(link);
            } else {
                const span = document.createElement('span');
                span.textContent = part.text;
                input.appendChild(span);
            }
        });

        tempDiv.appendChild(label);
        tempDiv.appendChild(input);
    });

    document.body.appendChild(tempDiv);
    viewWindowHeight = tempDiv.offsetHeight + 40; // handleViewRow의 창 높이
    document.body.removeChild(tempDiv);
    viewWindow.close();

    // Open a new window for editing
    const editWindow = window.open('', 'Edit Row', `width=1000,height=${viewWindowHeight}`);
    editWindow.document.body.innerHTML = '';
    const form = document.createElement('form');

    // Preview button
    const previewButton = document.createElement('button');
    previewButton.textContent = '미리보기';
    previewButton.type = 'button';
    previewButton.style.display = 'block'; // 이 속성을 추가하면 버튼이 새 줄에서 시작됩니다.
    previewButton.addEventListener('click', () => {
        const editedRowData = [];
        editWindow.document.querySelectorAll('textarea').forEach(input => {
            editedRowData.push(input.value);
        });
        updateRow(rowIndex, editedRowData);
        editWindow.close(); // Close the edit window
        handleViewRow(rowIndex); // Open the view window
    });
    form.appendChild(previewButton);

    let maxNumLines = 0;

    rowData.forEach((cellData, cellIndex) => {
        const label = document.createElement('label');
        label.textContent = `${columnTitles[cellIndex]}: `;
        const input = document.createElement('textarea');
        input.value = cellData;
        input.dataset.rowIndex = rowIndex;
        input.dataset.cellIndex = cellIndex;
        input.style.width = '95%';
        input.style.overflowY = 'hidden';

        const numLines = cellData.split('\n').length;
        maxNumLines = Math.max(maxNumLines, numLines);

        // Create a temporary div to measure the height of the textarea content
        const tempTextArea = document.createElement('textarea');
        tempTextArea.style.height = 'auto';
        tempTextArea.style.width = '95%';
        tempTextArea.style.visibility = 'hidden';
        tempTextArea.value = cellData;
        document.body.appendChild(tempTextArea);
        const optimalHeight = tempTextArea.scrollHeight;
        document.body.removeChild(tempTextArea);

        input.style.height = optimalHeight + 'px';

        input.addEventListener('input', function() {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
            adjustWindowHeight(); // Adjust window height on input
        });

        label.appendChild(input);
        form.appendChild(label);
    });

    const saveButton = document.createElement('button');
    saveButton.textContent = 'Save';
    saveButton.addEventListener('click', () => {
        const editedRowData = [];
        editWindow.document.querySelectorAll('textarea').forEach(input => {
            editedRowData.push(input.value);
        });
        updateRow(rowIndex, editedRowData);
        editWindow.close();
    });
    form.appendChild(saveButton);
    editWindow.document.body.appendChild(form);

    // Adjust window size based on content
    function adjustWindowHeight() {
        const currentScrollHeight = editWindow.document.body.scrollHeight;
        const windowHeight = window.innerHeight;
        const newHeight = currentScrollHeight + 40;

        if (newHeight > windowHeight) {
            editWindow.document.querySelectorAll('textarea').forEach(input => {
                input.style.overflowY = 'scroll';
            });
        } else {
            editWindow.document.querySelectorAll('textarea').forEach(input => {
                input.style.overflowY = 'hidden';
            });
        }

        editWindow.resizeTo(editWindow.outerWidth, Math.min(newHeight, windowHeight));
    }

    adjustWindowHeight(); // 처음 열 때 창 높이 조절
}

function adjustWindowHeight(editWindow) {
    const newHeight = editWindow.document.body.scrollHeight + 40; // 40px 추가 여유
    editWindow.resizeTo(editWindow.outerWidth, newHeight);
}

document.getElementById('addRowButton').addEventListener('click', handleAddRow);
function handleAddRow() {
	const newRow = new Array(columnTitles.length).fill('0');
	csvData.push(newRow);
    displayCSV();
}

function updateRow(rowIndex, editedRowData) {
    csvData[rowIndex] = editedRowData;
    displayCSV();
}

function handleDeleteRow(rowIndex) {
    // Remove the row from the CSV data
    csvData.splice(rowIndex, 1);
    // Update the displayed CSV
    displayCSV();
}

function handleCopyRow(rowIndex) {
    // Copy the row data
    const rowData = csvData[rowIndex];
    const newRow = rowData.slice(); // Create a shallow copy of the row
    csvData.push(newRow); // Add the copied row to the CSV data
    displayCSV(); // Update the displayed CSV
}

// Event Listener for Save Button
document.getElementById('saveButton').addEventListener('click', function() {
    const filename = prompt("Enter a filename to save:", loadedFileName);
    if (filename) {
        saveCSV(filename, csvData);
    }
});

function saveCSV(filename, data) {
    // Combine column titles with data rows
    const csvContent = [columnTitles.join(',')];
    data.forEach(row => {
        const rowWithLineBreaks = row.map(cell => cell.replace(/\n/g, '\\n')); // Replace line breaks with escape sequences
		const rowWithComma = rowWithLineBreaks.map(cell => cell.replace(/,/g,'","')); // Replace comma with double-quotation
        csvContent.push(rowWithComma.join(','));
    });

    const blob = new Blob([csvContent.join('\r\n')], { type: 'text/csv;charset=utf-8;' });
    
    // Extract filename without extension
    const filenameWithoutExtension = filename.split('.').slice(0, -1).join('.');
    
    // Create a new filename by combining the path, filename without extension, and extension
    const newFilename = `${filenameWithoutExtension}.csv`;

    const link = document.createElement('a');
    link.setAttribute('href', window.URL.createObjectURL(blob));
    link.setAttribute('download', newFilename); // Use the new filename
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
